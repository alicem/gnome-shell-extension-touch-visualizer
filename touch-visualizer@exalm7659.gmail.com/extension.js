const Clutter = imports.gi.Clutter;
const St = imports.gi.St;
const Main = imports.ui.main;
const Tweener = imports.ui.tweener;

let _sequenceActors = {};

let _touchEventId = null;

// FIXME: this is a horrible hack, but I didn't find other
// ways to compare event sequences, because they get copied
function _seqId(seq) {
    // Sample: [boxed instance wrapper GIName:Clutter.EventSequence jsobj@0x7fa862ef30a0 native@0x62]
    let str = seq.toString();

    str = str.split(' ');

    // native@0x62]
    str = str[str.length - 1];

    // 0x62
    str = str.substring(7, str.length - 1);

    return parseInt(str);
}

function _createActor(event) {
    let seq = _seqId(event.get_event_sequence());
    let [x, y] = event.get_coords();

    let actor = new St.Icon({ icon_name: 'media-record-symbolic', style_class: 'touch-indicator' });
    actor.set_position(x - actor.get_width() / 2, y - actor.get_height() / 2);
    _sequenceActors[seq] = actor;
    Main.uiGroup.add_actor(actor);
    actor.opacity = 0;

    Tweener.addTween(actor,
                     { opacity: 255,
                       time: 0.1,
                       transition: 'easeOutQuad' });
}

function _updateActor(event) {
    let [x, y] = event.get_coords();
    let seq = _seqId(event.get_event_sequence());

    let actor = _sequenceActors[seq];
    actor.set_position(x - actor.get_width() / 2, y - actor.get_height() / 2);
}

function _deleteActor(event, cancel) {
    let seq = _seqId(event.get_event_sequence());

    let actor = _sequenceActors[seq];

    if (cancel)
        _deleteActorComplete(actor, seq);
    else
        Tweener.addTween(actor,
                         { opacity: 0,
                           time: 0.1,
                           transition: 'easeInQuad',
                           onComplete: _deleteActorComplete,
                           onCompleteParams: [actor, seq] });
}

function _deleteActorComplete(actor, seq) {
    Main.uiGroup.remove_actor(actor);
    delete _sequenceActors[seq];
}

function _handleEvent(actor, event) {
    if (event.type() == Clutter.EventType.TOUCH_BEGIN)
        _createActor(event);
    else if (event.type() == Clutter.EventType.TOUCH_UPDATE)
        _updateActor(event);
    else if (event.type() == Clutter.EventType.TOUCH_END)
        _deleteActor(event, false)
    else if (event.type() == Clutter.EventType.TOUCH_CANCEL)
        _deleteActor(event, true)

    return Clutter.EVENT_PROPAGATE;
}

function init() {
}

function enable() {
    _touchEventId = global.stage.connect('captured-event', _handleEvent);
}

function disable() {
    global.stage.disconnect(_touchEventId);
    _touchEventId = null;

    for (let seq in _sequenceActors)
        Main.uiGroup.remove_actor(_sequenceActors[seq]);
    _sequenceActors = {};
}
